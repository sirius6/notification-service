# Configuration

The application uses an `application.yml` file containing the configuration. It must be placed in the same
directory with the application executable.

```yaml
spring:
  rabbitmq:
    host: localhost
    port: 5672
    username: user
    password: pwd

notification:
  maxTriesCount: 5
  rabbit:
    prefix: sirius
```

## RabbitMQ configuration

| Key                                             | Type   | Required | Default value | Description                                   |
|-------------------------------------------------|--------|----------|---------------|-----------------------------------------------|
| spring.rabbitmq.host                            | String | true     | localhost     | The host where is deployed RabbitMQ           |
| spring.rabbitmq.port                            | Int    | true     | 5672          | RabbitMQ port                                 |
| spring.rabbitmq.username                        | String | false    |               | The username to access RabbitMQ               |
| spring.rabbitmq.password                        | String | false    |               | The password to access RabbitMQ               |
| spring.rabbitmq.listener.simple.max-concurrency | Int    | false    | 3             | Max consumers count for each notification type|


## Notification configuration

| Key                            | Type   | Required | Default value            | Description                                              |
|--------------------------------|--------|----------|--------------------------|----------------------------------------------------------|
| notification.maxTriesCount     | Int    | true     | 5                        | The max tries before stopping notification retries       |
| notification.descriptorPath    | String | true     | classpath:providers.yml  | The path to the providers.yml file (prefix with `file:`) |
| notification.rabbit.prefix     | String | true     | sirius                   | The prefix used to name exchanges and queues             |
| notification.stomp.host        | String | true     | localhost                | The STOMP relay host for websocket                       |
| notification.stomp.port        | Int    | true     | 61613                    | The STOMP relay port for websocket                       |
| notification.stomp.login       | String | true     | guest                    | The STOMP relay login for websocket                      |
| notification.stomp.password    | String | true     | guest                    | The STOMP relay password for websocket                   |
| notification.api.login         | String | true     | sirius                   | The default API login                                    |
| notification.api.password      | String | true     | sirius                   | The default API password                                 |

For the STOMP relay you can use RabbitMQ. For this, you have to:

* enable the [STOMP plugin](https://www.rabbitmq.com/stomp.html)
* use the RabbitMQ host in `notification.stomp.host`. By default, the credentials are `guest:guest` (as defined in the previous table)


## Customize logs

By default logs are configured, but you can customize it by adding a file names `logback-spring.xml` in the application directory.

The logging system uses [Logback](http://logback.qos.ch/). Here are the MDC values available:

| Key              | Description                                                      |
|------------------|------------------------------------------------------------------|
| notificationId   | The notification identifier defined during notification creation |
| notificationType | The notification type (`EMAIL`, `SMS`, `WEBHOOK` or `WEBSOCKET`) |