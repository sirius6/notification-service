package io.sirius.notification

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

sealed class NotificationException(msg: String, th: Throwable? = null) : Exception(msg, th)

@ResponseStatus(HttpStatus.PAYMENT_REQUIRED)
class PaymentRequiredException(msg: String) : NotificationException(msg)

@ResponseStatus(HttpStatus.FORBIDDEN)
class AccessRestrictedException(msg: String) : NotificationException(msg)

@ResponseStatus(HttpStatus.PAYLOAD_TOO_LARGE)
class EntityTooLargeException(msg: String) : NotificationException(msg)

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
class NotificationSendingException(msg: String) : NotificationException(msg)

@ResponseStatus(HttpStatus.BAD_REQUEST)
class InvalidParameterException(
    msg: String,
    val paramName: String,
    th: Throwable? = null
): NotificationException(msg, th)