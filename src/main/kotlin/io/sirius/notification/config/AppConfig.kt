package io.sirius.notification.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.readValue
import io.sirius.notification.providers.NotificationProvider
import io.sirius.notification.providers.ProvidersDescriptor
import io.sirius.notification.providers.emailing.EmailingProvider
import io.sirius.notification.providers.sms.SmsProvider
import io.sirius.notification.services.ProviderService
import io.sirius.notification.services.impl.ProviderServiceImpl
import io.sirius.notification.util.findDuplicates
import org.apache.http.client.config.RequestConfig
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder

@Configuration
@EnableConfigurationProperties(NotificationProperties::class)
class AppConfig(
    private val properties: NotificationProperties,
    private val objectMapper: ObjectMapper
) {

    @Bean(destroyMethod = "close")
    fun httpClient(): CloseableHttpClient {
        val config = RequestConfig.custom()
            .setConnectTimeout(properties.http.connectTimeout)
            .setSocketTimeout(properties.http.readTimeout)
            .build()
        return HttpClientBuilder.create()
            .setDefaultRequestConfig(config)
            .setMaxConnPerRoute(properties.http.maxConnexionPerRoute)
            .build()
    }

    private fun yamlMapper(): ObjectMapper = Jackson2ObjectMapperBuilder()
        .factory(YAMLFactory())
        .build()

    @Bean
    fun providersDescriptor(): ProvidersDescriptor =
        properties.descriptorPath.inputStream.use { yamlMapper().readValue(it) }

    @Bean
    fun emailingProviderService(httpClient: CloseableHttpClient): ProviderService<EmailingProvider> {
        val providers: List<EmailingProvider> = providersDescriptor().emailing
            .map { it.createProvider(httpClient, objectMapper) }
        providers.validate()
        return ProviderServiceImpl(providers)
    }

    @Bean
    fun smsProviderService(httpClient: CloseableHttpClient): ProviderService<SmsProvider> {
        val providers: List<SmsProvider> = providersDescriptor().sms
            .map { it.createProvider(httpClient, objectMapper) }
        providers.validate()
        return ProviderServiceImpl(providers)
    }

    private fun List<NotificationProvider>.validate() {
        val duplicatesProviders = this.map { it.id }.findDuplicates()
        require(duplicatesProviders.isEmpty()) { "Duplicates providers found: $duplicatesProviders" }
    }

}