package io.sirius.notification.config

import org.springframework.context.annotation.Configuration
import org.springframework.messaging.simp.config.MessageBrokerRegistry
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker
import org.springframework.web.socket.config.annotation.StompEndpointRegistry
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer
import org.springframework.web.socket.server.standard.TomcatRequestUpgradeStrategy
import org.springframework.web.socket.server.support.DefaultHandshakeHandler


@Configuration
@EnableWebSocketMessageBroker
class WebSocketConfig(private val properties: NotificationProperties) : WebSocketMessageBrokerConfigurer {

    override fun registerStompEndpoints(registry: StompEndpointRegistry) {
        // SockJS websocket
        registry.addEndpoint("/ws").withSockJS()

        // Classic websocket
        val upgradeStrategy = TomcatRequestUpgradeStrategy()
        registry.addEndpoint("/ws").setHandshakeHandler(DefaultHandshakeHandler(upgradeStrategy))
    }

    override fun configureMessageBroker(registry: MessageBrokerRegistry) {
        registry.setApplicationDestinationPrefixes("/app")
        registry.enableStompBrokerRelay("/topic", "/user/queue")
            .setRelayHost(properties.stomp.host)
            .setRelayPort(properties.stomp.port)
            .setClientLogin(properties.stomp.login)
            .setClientPasscode(properties.stomp.password)
        registry.setUserDestinationPrefix("/user")
    }
}