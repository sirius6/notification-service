package io.sirius.notification.controller

import org.springframework.http.*
import org.springframework.validation.FieldError
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler


@RestControllerAdvice
class RestErrorHandler : ResponseEntityExceptionHandler() {

    override fun handleMethodArgumentNotValid(
        ex: MethodArgumentNotValidException,
        headers: HttpHeaders,
        status: HttpStatusCode,
        request: WebRequest
    ): ResponseEntity<Any> {
        val invalidFields = ex.bindingResult.allErrors.map { error ->
            require(error is FieldError)
            error.field
        }.toSet()

        return buildResponse(status, ErrorPayload("Invalid parameters", invalidFields))
    }

    override fun handleExceptionInternal(
        ex: Exception,
        body: Any?,
        headers: HttpHeaders,
        status: HttpStatusCode,
        request: WebRequest
    ): ResponseEntity<Any> {
        val message: String = if (status.is5xxServerError) {
            logger.error("Server error (status=${status.value()})", ex)
            "Server error"
        } else {
            logger.warn("Server warning (status=${status.value()}, msg=${ex.message})")
            ex.message ?: "Server error"
        }
        return buildResponse(status, ErrorPayload(message))
    }

    private fun buildResponse(
        status: HttpStatusCode,
        payload: ErrorPayload
    ): ResponseEntity<Any> = ResponseEntity.status(status)
        .contentType(MediaType.APPLICATION_JSON)
        .body(payload)

    internal data class ErrorPayload(
        val message: String,
        val invalidFields: Set<String>? = null
    )
}