package io.sirius.notification.model

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import io.sirius.notification.model.databind.TimestampDeserializer
import io.sirius.notification.model.databind.TimestampSerializer
import io.sirius.notification.util.toProviderCallbackInfo
import java.time.Instant

data class MandrillEvent(
    @JsonDeserialize(using = TimestampDeserializer::class)
    @JsonSerialize(using = TimestampSerializer::class)
    val ts: Instant,
    val event: Type,
    val ip: String?,
    val user_agent: String?,
    val url: String?,
    val msg: Message
) {

    fun toTrackingEvent() = TrackingEvent(
        notification = NotificationCallback.NotificationCallbackInfo(
            id = msg.metadata.notificationId,
            type = NotificationInfo.Type.EMAIL
        ),
        metadata = msg.metadata.metadata,
        eventType = event.trackingType,
        provider = msg.metadata.toProviderCallbackInfo(),
        timestamp = ts,
        ipAddress = ip,
        userAgent = user_agent,
        email = msg.email,
        clickedUrl = url
    )

    enum class Type(val trackingType: TrackingEvent.Type) {
        send(TrackingEvent.Type.SENT),
        deferral(TrackingEvent.Type.DEFERRED),
        hard_bounce(TrackingEvent.Type.BOUNCE),
        soft_bounce(TrackingEvent.Type.BOUNCE),
        `open`(TrackingEvent.Type.OPEN),
        click(TrackingEvent.Type.CLICK),
        spam(TrackingEvent.Type.SPAM),
        unsub(TrackingEvent.Type.UNSUB),
        reject(TrackingEvent.Type.BLOCKED)
    }

    data class Message(
        val email: String,
        val state: String,
        val metadata: ProviderMetadata
    )
}