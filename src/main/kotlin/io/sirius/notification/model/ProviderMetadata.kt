package io.sirius.notification.model

import com.fasterxml.jackson.annotation.JsonAnyGetter
import com.fasterxml.jackson.annotation.JsonAnySetter

/**
 * Represents metadata send to a provider (Mandrill, Mailjet, etc.)
 */
data class ProviderMetadata(
    val notificationId: String,
    val providerId: String?,
    val providerType: String?
) {

    @JsonAnySetter
    @get:JsonAnyGetter
    var metadata: Map<String, String> = mutableMapOf()

}