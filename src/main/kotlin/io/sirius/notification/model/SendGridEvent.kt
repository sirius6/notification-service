package io.sirius.notification.model

import com.fasterxml.jackson.annotation.JsonAnyGetter
import com.fasterxml.jackson.annotation.JsonAnySetter
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import io.sirius.notification.model.databind.TimestampDeserializer
import io.sirius.notification.model.databind.TimestampSerializer
import io.sirius.notification.util.toProviderCallbackInfo
import java.time.Instant

data class SendGridEvent(
    val email: String,
    @JsonDeserialize(using = TimestampDeserializer::class)
    @JsonSerialize(using = TimestampSerializer::class)
    val timestamp: Instant,
    val event: Type,
    val ip: String?,
    val useragent: String?,
    val url: String?,
    val category: List<String>?,

    @get:JsonAnyGetter
    @JsonAnySetter
    var metadata: Map<String, String> = mutableMapOf()
) {

    private fun getProviderMetadata(): ProviderMetadata {
        val providerMetadata = ProviderMetadata(
            notificationId = metadata["notificationId"]!!,
            providerId =  metadata["providerId"]!!,
            providerType =  metadata["providerType"]!!
        )
        providerMetadata.metadata = metadata
            .filterKeys { key -> key.startsWith("metadata_") }
            .mapKeys { (key, _) -> key.substring(9) }
        return providerMetadata
    }

    fun toTrackingEvent(): TrackingEvent? {
        if (event.trackingType == null) {
            return null
        }
        val providerMetadata = getProviderMetadata()

        return TrackingEvent(
            notification = NotificationCallback.NotificationCallbackInfo(
                id = providerMetadata.notificationId,
                type = NotificationInfo.Type.EMAIL
            ),
            metadata = providerMetadata.metadata,
            eventType = event.trackingType,
            provider = providerMetadata.toProviderCallbackInfo(),
            timestamp = timestamp,
            ipAddress = ip,
            userAgent = useragent,
            email = email,
            clickedUrl = url
        )
    }

    enum class Type(val trackingType: TrackingEvent.Type? = null) {
        deferred(TrackingEvent.Type.DEFERRED),
        delivered(TrackingEvent.Type.SENT),
        `open`(TrackingEvent.Type.OPEN),
        click(TrackingEvent.Type.CLICK),
        bounce(TrackingEvent.Type.BOUNCE),
        spamreport(TrackingEvent.Type.SPAM),
        unsubscribe(TrackingEvent.Type.UNSUB),
        group_unsubscribe(TrackingEvent.Type.UNSUB),

        // Unsupported events
        processed,
        dropped,
        group_resubscribe
    }

}