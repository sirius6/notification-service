package io.sirius.notification.model.databind

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import java.time.Instant

class TimestampSerializer : JsonSerializer<Instant>() {

    override fun serialize(
        instant: Instant,
        gen: JsonGenerator,
        provider: SerializerProvider
    ) {
        gen.writeNumber(instant.epochSecond)
    }
}

class TimestampDeserializer : JsonDeserializer<Instant>() {

    override fun deserialize(
        parser: JsonParser,
        context: DeserializationContext
    ): Instant = Instant.ofEpochSecond(parser.longValue)
}