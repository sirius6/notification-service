package io.sirius.notification.providers

interface NotificationProvider {
    val id: String
    val provider: String
}