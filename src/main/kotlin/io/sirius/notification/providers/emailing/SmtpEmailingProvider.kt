package io.sirius.notification.providers.emailing

import com.fasterxml.jackson.databind.ObjectMapper
import io.sirius.notification.model.EmailInfo
import io.sirius.notification.providers.ProviderConfig
import io.sirius.notification.util.logger
import org.apache.http.impl.client.CloseableHttpClient
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.JavaMailSenderImpl
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.mail.javamail.MimeMessagePreparator
import java.nio.charset.StandardCharsets
import jakarta.mail.internet.InternetAddress
import jakarta.mail.internet.MimeMessage
import jakarta.mail.util.ByteArrayDataSource

class SmtpEmailingProvider(
    override val provider: String,
    override val id: String = provider,
    private val sender: JavaMailSender
) : EmailingProvider {

    private val logger = logger()

    override fun sendEmail(emailInfo: EmailInfo) {
        sender.send(EmailNotificationPreparator(emailInfo))

        if (sender is JavaMailSenderImpl) {
            logger.info("Email sent to SMTP {}", sender.host)
        }
    }

    private class EmailNotificationPreparator(private val emailInfo: EmailInfo) : MimeMessagePreparator {

        override fun prepare(mimeMessage: MimeMessage) {
            val helper = MimeMessageHelper(mimeMessage, emailInfo.isMultipart())
            helper.setFrom(emailInfo.from.toInternetAddress())
            helper.setSubject(emailInfo.subject)
            helper.setTo(emailInfo.to.map { it.toInternetAddress() }.toTypedArray())
            helper.setCc(emailInfo.cc.map { it.toInternetAddress() }.toTypedArray())
            helper.setBcc(emailInfo.bcc.map { it.toInternetAddress() }.toTypedArray())

            if (emailInfo.plainText != null && emailInfo.htmlText != null) {
                helper.setText(emailInfo.plainText, emailInfo.htmlText)
            } else if (emailInfo.plainText != null) {
                helper.setText(emailInfo.plainText, false)
            } else if (emailInfo.htmlText != null) {
                helper.setText(emailInfo.htmlText, true)
            }

            // Add attachments
            emailInfo.attachments.forEach {
                helper.addAttachment(
                    it.filename,
                    ByteArrayDataSource(it.data, it.contentType)
                )
            }
        }

        private fun EmailInfo.EmailAddress.toInternetAddress(): InternetAddress =
            InternetAddress(email, personal, StandardCharsets.UTF_8.name())

        private fun EmailInfo.isMultipart(): Boolean =
            attachments.isNotEmpty() || (plainText != null && htmlText != null)
    }
}

data class SmtpProviderConfig(
    override val provider: String,
    override val id: String = provider,
    val host: String,
    val port: Int,
    val username: String? = null,
    val password: String? = null,
    val connectTimeout: Int = 5_000,
    val readTimeout: Int = 30_000,
) : ProviderConfig<SmtpEmailingProvider> {

    override fun createProvider(
        httpClient: CloseableHttpClient,
        objectMapper: ObjectMapper
    ): SmtpEmailingProvider {
        val sender = JavaMailSenderImpl()
        sender.host = host
        sender.port = port
        sender.username = username
        sender.password = password
        sender.protocol = "smtp"
        sender.defaultEncoding = StandardCharsets.UTF_8.name()

        // Other properties
        sender.javaMailProperties["mail.smtp.connectiontimeout"] = connectTimeout
        sender.javaMailProperties["mail.smtp.timeout"] = readTimeout

        return SmtpEmailingProvider(
            id = id,
            provider = provider,
            sender = sender
        )
    }
}