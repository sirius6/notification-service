package io.sirius.notification.services

import org.springframework.amqp.core.Message

interface DeadLetterService {

    fun onFailedNotification(failedMessage: Message)

}