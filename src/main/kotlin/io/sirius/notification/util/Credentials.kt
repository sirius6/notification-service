package io.sirius.notification.util

import org.apache.http.Header
import org.apache.http.HttpHeaders
import org.apache.http.message.BasicHeader
import java.nio.charset.StandardCharsets
import java.util.*

interface Credentials {
    fun toHeader(): Header
}

data class PasswordCredentials(val login: String, val password: String) : Credentials {
    override fun toHeader(): Header {
        val data = "$login:$password".toByteArray(StandardCharsets.US_ASCII)
        val encoded = Base64.getEncoder().encodeToString(data)
        return BasicHeader(HttpHeaders.AUTHORIZATION, "Basic $encoded")
    }
}

data class BearerCredentials(val value: String) : Credentials {
    override fun toHeader(): Header = BasicHeader(HttpHeaders.AUTHORIZATION, "Bearer $value")
}