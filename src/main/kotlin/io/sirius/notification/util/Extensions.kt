package io.sirius.notification.util

import com.fasterxml.jackson.databind.ObjectMapper
import io.sirius.notification.model.NotificationCallback
import io.sirius.notification.model.NotificationInfo
import io.sirius.notification.model.ProviderMetadata
import io.sirius.notification.model.TrackingEvent
import io.sirius.notification.providers.NotificationProvider
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.MDC
import org.springframework.amqp.core.Message
import org.springframework.core.io.InputStreamSource
import java.io.Reader
import java.util.*


fun Any.logger(clazz: Class<*> = javaClass): Logger = LoggerFactory.getLogger(clazz)

fun ByteArray.toBase64(): String = Base64.getEncoder().encodeToString(this)

fun InputStreamSource.toByteArray(): ByteArray = inputStream.use { it.readAllBytes() }

fun Map<String, List<String>>.getFirst(key: String): String? {
    val values: List<String> = get(key) ?: return null
    return values.getOrNull(0)
}

fun NotificationInfo.toProviderMetadata(provider: NotificationProvider): ProviderMetadata {
    val providerMetadata = ProviderMetadata(
        notificationId = id,
        providerId = provider.id,
        providerType = provider.provider
    )
    providerMetadata.metadata = metadata
    return providerMetadata
}

fun ProviderMetadata.toProviderCallbackInfo(): NotificationCallback.ProviderCallbackInfo? {
    return if (providerId != null && providerType != null) {
        NotificationCallback.ProviderCallbackInfo(id = providerId, type = providerType)
    } else {
        null
    }
}

fun Enum<*>.toRabbitToken(): String = name

inline fun <reified T> ObjectMapper.readList(body: String): List<T> {
    val type = typeFactory.constructParametricType(List::class.java, T::class.java)
    return readValue(body, type)
}

inline fun <reified T> ObjectMapper.readList(reader: Reader): List<T> {
    val type = typeFactory.constructParametricType(List::class.java, T::class.java)
    return readValue(reader, type)
}

fun <T> List<T>.findDuplicates(): Set<T> {
    val copy = mutableSetOf<T>()
    return this.filter { !copy.add(it) }.toSet()
}

/** Execute code in a [MDC] context. It is cleared at the end. */
fun <T> mdc(action: () -> T): T {
    try {
        return action()
    } finally {
        MDC.clear()
    }
}

fun NotificationInfo.mdc() {
    mdc("notificationId" to id)
    mdc("notificationType" to type.name)
    metadata.forEach { mdc(it.key to it.value) }
}

fun TrackingEvent.mdc() {
    mdc("notificationId" to notification.id)
    mdc("notificationType" to notification.type.name)
    metadata.forEach { mdc(it.key to it.value) }
}

private fun mdc(pair: Pair<String, Any?>) {
    if (pair.second != null) {
        MDC.put(pair.first, pair.second.toString())
    } else {
        MDC.remove(pair.first)
    }
}

@Suppress("UNCHECKED_CAST")
fun List<Map<String, *>>.toDeathHeaders(): List<DeathHeader> {
    return this.map { header ->
        DeathHeader(
            reason = header["reason"] as String,
            count = (header["count"] as Long).toInt(),
            exchange = header["exchange"] as String,
            queue = header["queue"] as String,
            routingKeys = (header["routing-keys"] as List<String>).toSet()
        )
    }
}

fun Message.getDeathHeaders(): List<DeathHeader> = messageProperties.xDeathHeader?.toDeathHeaders() ?: emptyList()

fun List<Map<String, *>>?.getTryIndex(): Int = this?.toDeathHeaders()?.firstOrNull()?.count ?: 0