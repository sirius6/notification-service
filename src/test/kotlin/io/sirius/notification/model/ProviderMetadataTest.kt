package io.sirius.notification.model

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder

class ProviderMetadataTest {

    private lateinit var objectMapper: ObjectMapper

    @BeforeEach
    fun before() {
        objectMapper = Jackson2ObjectMapperBuilder().build()
    }

    @Test
    fun testMetadataSerialization() {
        val metadata = ProviderMetadata("notifId", "provId", "provType")
        metadata.metadata = mapOf(
            "key1" to "value1",
            "key2" to "value2"
        )

        val str = objectMapper.writeValueAsString(metadata)
        assertThat(str).isEqualTo("""{"notificationId":"notifId","providerId":"provId","providerType":"provType","key1":"value1","key2":"value2"}""")

        val result = objectMapper.readValue<ProviderMetadata>(str)
        assertThat(result).isEqualTo(metadata)
        assertThat(result.metadata).isEqualTo(metadata.metadata)
    }
}