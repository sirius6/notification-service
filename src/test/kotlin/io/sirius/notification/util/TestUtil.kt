package io.sirius.notification.util

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.http.client.config.RequestConfig
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.assertj.core.api.AbstractAssert
import org.assertj.core.api.AbstractIterableAssert
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import java.util.function.Consumer

object TestUtil {

    fun objectMapper(): ObjectMapper {
        val builder = Jackson2ObjectMapperBuilder()
        builder.serializationInclusion(JsonInclude.Include.NON_NULL)
        return builder.build()
    }

    fun httpClient(): CloseableHttpClient {
        val config = RequestConfig.custom()
            .setConnectTimeout(2_000) // 2 seconds
            .setSocketTimeout(10_000) // 10 seconds
            .build()
        return HttpClientBuilder.create()
            .setDefaultRequestConfig(config)
            .build()
    }

    fun <T : Any> AbstractAssert<*, T>.satisfiesRequirements(requirements: (T) -> Unit): AbstractAssert<*, *> {
        return this.satisfies(requirements.toConsumer())
    }

    fun <E : Any, I : Iterable<E>> AbstractIterableAssert<*, I, E, *>.allSatisfyRequirements(requirements: (E) -> Unit): AbstractIterableAssert<*, *, *, *> {
        return this.allSatisfy(requirements.toConsumer())
    }

    private fun <T> ((T) -> Unit).toConsumer(): Consumer<T> {
        return Consumer { this(it) }
    }
}