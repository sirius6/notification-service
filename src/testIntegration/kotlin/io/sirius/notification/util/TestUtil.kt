package io.sirius.notification.util

import org.assertj.core.api.AbstractAssert
import org.assertj.core.api.AbstractIterableAssert
import java.util.function.Consumer

object TestUtil {

    fun <T : Any> AbstractAssert<*, T>.satisfiesRequirements(requirements: (T) -> Unit): AbstractAssert<*, *> {
        return this.satisfies(requirements.toConsumer())
    }

    fun <E : Any, I : Iterable<E>> AbstractIterableAssert<*, I, E, *>.allSatisfyRequirements(requirements: (E) -> Unit): AbstractIterableAssert<*, *, *, *> {
        return this.allSatisfy(requirements.toConsumer())
    }

    private fun <T> ((T) -> Unit).toConsumer(): Consumer<T> {
        return Consumer { this(it) }
    }
}